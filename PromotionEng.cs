
using PromotionEngine.SKU_IDs;
using System;
using Xunit;

namespace PromotionEngineTests
{
    public class PromotionEng
    {
        [Fact]
        public void SKU_ID_A_UnitPriceCalculationShouldReturn50()
        {
            //Arrange
            int expected = 50;
            int number = 1;
            SKU_ID_A sKU_ID_A = new SKU_ID_A(number);

            //Act
            int actual = sKU_ID_A.CalculatingValue();

            //Assert
            Assert.Equal(actual, expected);
        }

        [Fact]
        public void SKU_ID_A_ActivePromotionCalculationShouldReturn130For3A()
        {
            //Arrange
            int expected = 130;
            int number = 3;
            SKU_ID_A sKU_ID_A = new SKU_ID_A(number);

            //Act
            int actual = sKU_ID_A.CalculatingValue();

            //Assert
            Assert.Equal(actual, expected);
        }

        [Fact]
        public void SKU_ID_B_UnitPriceCalculationShouldReturn30()
        {
            //Arrange
            int expected = 30;
            int number = 1;
            SKU_ID_B sKU_ID_B = new SKU_ID_B(number);

            //Act
            int actual = sKU_ID_B.CalculatingValue();

            //Assert
            Assert.Equal(actual, expected);
        }

        [Fact]
        public void SKU_ID_B_ActivePromotionCalculationShouldReturn30For2B()
        {
            //Arrange
            int expected = 45;
            int number = 2;
            SKU_ID_B sKU_ID_B = new SKU_ID_B(number);

            //Act
            int actual = sKU_ID_B.CalculatingValue();

            //Assert
            Assert.Equal(actual, expected);
        }

        [Fact]
        public void SKU_ID_C_UnitPriceCalculationShouldReturn20()
        {
            //Arrange
            int expected = 20;
            int numberOfC = 1;
            int numberOfD = 0;
            SKU_ID_CD sKU_ID_CD = new SKU_ID_CD(numberOfC, numberOfD);

            //Act
            int actual = sKU_ID_CD.CalculatingValue();

            //Assert
            Assert.Equal(actual, expected);
        }

        [Fact]
        public void SKU_ID_D_UnitPriceCalculationShouldReturn15()
        {
            //Arrange
            int expected = 15;
            int numberOfC = 0;
            int numberOfD = 1;
            SKU_ID_CD sKU_ID_CD = new SKU_ID_CD(numberOfC, numberOfD);

            //Act
            int actual = sKU_ID_CD.CalculatingValue();

            //Assert
            Assert.Equal(actual, expected);
        }

        [Fact]
        public void SKU_ID_CD_ActivePromotionCalculationShouldReturn30ForCD()
        {
            //Arrange
            int expected = 30;
            int numberOfC = 1;
            int numberOfD = 1;
            SKU_ID_CD sKU_ID_CD = new SKU_ID_CD(numberOfC, numberOfD);

            //Act
            int actual = sKU_ID_CD.CalculatingValue();

            //Assert
            Assert.Equal(actual, expected);
        }
    }
}

